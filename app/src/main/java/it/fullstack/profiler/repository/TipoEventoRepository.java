package it.fullstack.profiler.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.fullstack.profiler.domain.tipologiche.TipoEvento;

@Repository
public interface TipoEventoRepository extends JpaRepository<TipoEvento, Long> {
  TipoEvento findByDescrizione(String descrizione);
}
