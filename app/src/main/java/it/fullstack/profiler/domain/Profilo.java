package it.fullstack.profiler.domain;

import java.io.Serializable;
import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
public class Profilo extends AbstractAuditingEntity implements Serializable {
 
  private static final long serialVersionUID = 1L;

  @Id 
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private List<Ruolo> ruoli;

}
