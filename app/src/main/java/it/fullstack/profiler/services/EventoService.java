package it.fullstack.profiler.services;

import static java.lang.String.format;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fullstack.profiler.domain.Evento;
import it.fullstack.profiler.domain.tipologiche.TipoEvento;
import it.fullstack.profiler.repository.EventoRepository;
import it.fullstack.profiler.repository.TipoEventoRepository;
import it.fullstack.profiler.services.dto.CreateEventoDTO;
import it.fullstack.profiler.services.dto.UpdateEventoDTO;
import it.fullstack.profiler.services.mapper.EventoMapper;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
@AllArgsConstructor
public class EventoService {

  private final EventoRepository eventoRepository;
  private final TipoEventoRepository tipoEventoRepository;
  
  /**
   * Get one Evento by id.
   *
   * @param id the id of the entity.
   * @return the entity.
   */
  @Transactional(readOnly = true)
  public Optional<Evento> findOne(Long id) {
    log.debug("Request to get Evento:{}", id);
    
    return eventoRepository.findById(id);
  }
  
  /**
   * Create a Evento.
   *
   * @param dto the entity to save.
   * @return the updated entity.
   */
  public Evento create(CreateEventoDTO dto) {
    log.debug("Request to create Evento:{}", dto);
    
    TipoEvento tipo = tipoEventoRepository.findByDescrizione("Kafka");
    
    Evento toCreate = EventoMapper
        .toEntity(dto)
        .tipo(tipo)
        .ricevuto(LocalDateTime.now());
    
    return eventoRepository.save(toCreate);
  }
  
  /**
   * Update a Evento.
   *
   * @param dto the entity to save.
   * @return the updated entity.
   */
  public Evento update(UpdateEventoDTO dto) {
    log.debug("Request to update Evento:{}", dto);
    
    Evento toUpdate = eventoRepository.findById(dto.getId())
      .map(entity -> EventoMapper.toEntity(entity, dto))
      .orElseThrow(() -> new EntityNotFoundException(format("Evento non esistente: %s", dto.getId())));

    Evento saved = eventoRepository.save(toUpdate);
    
    return saved;
  }

  /**
   * Delete Evento by id.
   *
   * @param id the of the entity.
   */
  public void delete(Long id) {
    eventoRepository.deleteById(id);
  }

}
