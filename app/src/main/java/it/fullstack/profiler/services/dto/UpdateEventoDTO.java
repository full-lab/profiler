package it.fullstack.profiler.services.dto;

import it.fullstack.profiler.domain.tipologiche.TipoEvento;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class UpdateEventoDTO {
  
  @NotNull
  private Long id;

  @NotNull
  private TipoEvento tipo;

  @NotNull
  private String messaggio;
}
