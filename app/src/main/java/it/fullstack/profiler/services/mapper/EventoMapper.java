package it.fullstack.profiler.services.mapper;

import it.fullstack.profiler.domain.Evento;
import it.fullstack.profiler.services.dto.CreateEventoDTO;
import it.fullstack.profiler.services.dto.UpdateEventoDTO;

public interface EventoMapper {

  public static Evento toEntity(CreateEventoDTO dto) {
    return new Evento()
        .messaggio(dto.getMessaggio());
  }
  
  public static Evento toEntity(Evento evento, UpdateEventoDTO dto) {
    return evento
        .messaggio(dto.getMessaggio())
        .tipo(dto.getTipo());
  }

}
